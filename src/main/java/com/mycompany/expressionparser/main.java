/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.expressionparser;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author aamin
 */
public class main {
    public static void main(String[] args) {
        try(Scanner sc = new Scanner(System.in)) {
            ConsoleReader cr = new ConsoleReader();
            String expression = cr.readExpression(sc);
            Tokenizer tokenizer = new Tokenizer(expression);
            ArrayList<Token> list = new ArrayList<>(tokenizer.separate());
            ShuntAlg alg = new ShuntAlg();
            ArrayList<Token> resultList = new ArrayList<>(alg.converter(list));
            for(Token i : resultList) {
                System.out.println(i);
            }
            Evaluator ev = new Evaluator();
            System.out.println(ev.evaluate(resultList));
            
            
        } catch(IOException | ParseException e) {
            System.out.println(e.getMessage());
        }

    }
}
