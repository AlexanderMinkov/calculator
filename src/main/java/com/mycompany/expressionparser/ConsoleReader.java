/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.expressionparser;

import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author aamin
 */
public class ConsoleReader {
    public String readExpression(Scanner sc) throws IOException {
        System.out.println("Enter expression: ");
        if(sc.hasNextLine()) {
            return sc.nextLine().replace(" ","");
        }
        throw new IOException("Empty string input");
    }
}
