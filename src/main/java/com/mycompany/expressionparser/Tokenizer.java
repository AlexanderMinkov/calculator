/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.expressionparser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 *
 * @author aamin
 */
public class Tokenizer {
    private final String OPERATORS = "+-*/";
    private final String SEPARATOR = ",";
    private String expression = "";
    public Tokenizer(String expression) {
        this.expression = expression;
    }

    public static boolean isNumber(String str) {
        return str.matches("[-+]?[0-9]*\\.?[0-9]+");
    }
    public static boolean isFunction(String str) {
        return str.matches("(?i)min|max|sin|cos|pow{1,1}");
    }
    public static boolean isOperator(String str) {
        return str.matches("[+*-\\/]{1,1}");
    }
    public static boolean isDelimeter(String str) {
        return str.equals(",");
    }
    public static boolean isLeftPar(String str) {
        return str.equals("(");
    }
    public static boolean isRightPar(String str) {
        return str.equals(")");
    }
    public static String defineType(String str) throws IOException {
        if(isDelimeter(str)){
            return "Delimeter";
        }
        if(isFunction(str)){
            
            return "Function";
        }
        if(isLeftPar(str)){
            
            return "Left Par";
        }
        if(isRightPar(str)){
            return "Right Par";
        }
        if(isNumber(str)){
            return "Literal";
        }
        if(isOperator(str)){
            return "Operator";
        }
        return null;
        
    }
    public ArrayList<Token> separate() throws IOException {
        ArrayList<Token> list = new ArrayList<>();
        StringTokenizer stringTokenizer = new StringTokenizer(expression,
				OPERATORS + SEPARATOR + "()", true);

        while (stringTokenizer.hasMoreTokens()) {
            String token = stringTokenizer.nextToken();
            String type = defineType(token);
            if(type == null) {
                throw new IOException("Input value is not correct");
            }
            list.add(new Token(type,token));
        }
        return list;
    }
}