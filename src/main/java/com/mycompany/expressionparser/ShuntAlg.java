/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.expressionparser;

import java.io.IOException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 *
 * @author aamin
 */
public class ShuntAlg {
    
    private static Map<String, Integer> ops = new HashMap<String, Integer>() {{
        put("+", 1);
        put("-", 1);
        put("*", 2);
        put("/", 2);
    }};
    public ArrayDeque<Token> converter(ArrayList<Token> list) throws IOException {
        ArrayDeque<Token> queue = new ArrayDeque<>();
        Stack<Token> stack = new Stack<>();
        for(Token token : list) {
            if(token.getType() == "Literal") {
                queue.addLast(token);
            }
            if(token.getType() == "Function") {
                stack.add(token);
            }
            if (token.getType() == "Delimeter") {

                while (!stack.isEmpty() && stack.peek().getType() != "Left Par") {
                    if (stack.isEmpty()) {
                        throw new IOException("No open par");
                       
                    }
                    queue.addLast(stack.pop());
                }

            }
            if (token.getType() == "Operator") {

                while (!stack.isEmpty() && stack.peek().getType() == "Operator" && (ops.get(token.getValue()) <= ops.get(stack.peek().getValue()))) {
                    queue.addLast(stack.pop());
                    if (stack.isEmpty()) {
                        break;
                    }
                }

                stack.add(token);
            }
            if (token.getType() == "Left Par") {
                stack.add(token);
            }
            if (token.getType() == "Right Par") {

                while (!stack.isEmpty() && stack.peek().getType() != "Left Par") {
                    queue.addLast(stack.pop());
                    if (stack.isEmpty()) {
                        throw new IOException("No open par");
                        
                    }
                }
                if(!stack.isEmpty()) {
                    stack.pop();
                }
                if (!stack.isEmpty() && stack.peek().getType() == "Function") {
                    queue.addLast(stack.pop());
                }

            }
        }
        while(!stack.isEmpty()) {
            if(stack.peek().getType() == "Left Par" || stack.peek().getType() == "Right Par") {
                throw new IOException("No par");
                
            }
            queue.addLast(stack.pop());
        }
       
        return queue;
    } 
}