/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.expressionparser;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayDeque;
import java.util.ArrayList;

/**
 *
 * @author aamin
 */
public class Evaluator {
    public String opsEvaluation(String a, String b, String op) {
        float first = Float.parseFloat(a);
        float second = Float.parseFloat(b);
        switch(op) {
            case("+"):
                return String.valueOf(first+second);
            case("-"):
                return String.valueOf(first-second);
            case("*"):
                return String.valueOf(first*second);
            case("/"):
                return String.valueOf(first/second);
            default:
                return null;
        }
    }

    public String funcEvaluation(String op, String...par) {
        double result;
        switch (op) {
            case ("min"):
                result = Math.min(Float.parseFloat(par[0]), Float.parseFloat(par[1]));
                return String.valueOf(result);
            case ("max"):
                result = Math.max(Float.parseFloat(par[0]), Float.parseFloat(par[1]));
                return String.valueOf(result);
            case ("sin"):
                result = Math.sin(Double.parseDouble(par[0]));
                return String.valueOf(result);
            case ("cos"):
                result = Math.cos(Double.parseDouble(par[0]));
                return String.valueOf(result);
            case ("pow"):
                result = Math.pow(Float.parseFloat(par[0]), Float.parseFloat(par[1]));
                return String.valueOf(result);
            default:
                return null;
        }
    }
    public String evaluate(ArrayList<Token> list) throws IOException, ParseException {
        if(list.size() == 1) {
            return list.get(0).getValue();
        }
        for(int i = 0; i < list.size(); i++) {
            Token current = list.get(i);
            if(current.getType().equals("Function")) {
                if(current.getValue().equals("sin") || current.getValue().equals("cos")) {
                    if(i > 0 && list.get(i-1).getType().equals("Literal")) {
                        list.set(i-1, new Token("Literal",funcEvaluation(current.getValue(), list.get(i-1).getValue())));
                        list.remove(i);
                        return evaluate(list);
                        // add recursion here
                    } else {
                        throw new IOException("no Literal");
                    }
                } else {
                    if(i > 1 && (list.get(i-1).getType().equals("Literal") && list.get(i-2).getType().equals("Literal") )) {
                        list.set(i-1, new Token("Literal",funcEvaluation(current.getValue(), list.get(i-1).getValue(),list.get(i-2).getValue())));
                        list.remove(i);
                        list.remove(i-2);
                        return evaluate(list);
                        // add recursion here
                    } else {  
                        throw new IOException("No literal");
                    }
                }
            } else {
                if(current.getType().equals("Operator")) {
                    if(i > 1 && (list.get(i-1).getType().equals("Literal") && list.get(i-2).getType().equals("Literal") )) {
                        list.set(i-1, new Token("Literal",opsEvaluation(list.get(i-2).getValue(),list.get(i-1).getValue(),current.getValue())));
                        list.remove(i);
                        list.remove(i-2);
                        return evaluate(list);
                        
                        // add recursion here
                    } else {
                         throw new IOException("No literal");
                    }
                }
            } 
        }
        return null;
    }
}
