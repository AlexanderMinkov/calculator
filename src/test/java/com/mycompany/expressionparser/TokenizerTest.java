/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.expressionparser;

import java.util.ArrayList;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author aamin
 */
public class TokenizerTest {
    
    public TokenizerTest() {
    }


    /**
     * Test of isDigit method, of class Tokenizer.
     */
    @org.junit.Test
    public void testIsDigit() {
        System.out.println("isDigit");
        assertEquals(true, Tokenizer.isNumber("3"));
        assertEquals(true, Tokenizer.isNumber("53.67675"));
        assertEquals(true, Tokenizer.isNumber("-1.3"));
        assertEquals(true, Tokenizer.isNumber("+3.445"));
    }
    @org.junit.Test
    public void testIsFunction() {
        System.out.println("isDigit");
        assertEquals(true, Tokenizer.isFunction("Max"));
        assertEquals(true, Tokenizer.isFunction("pow"));
        assertEquals(true, Tokenizer.isFunction("cos"));
        assertEquals(true, Tokenizer.isFunction("COS"));
    }

    /**
     * Test of isOperator method, of class Tokenizer.
     */
    @org.junit.Test
    public void testIsOperator() {
        System.out.println("isOperator");
        assertEquals(true, Tokenizer.isOperator("+"));
        assertEquals(true, Tokenizer.isOperator("-"));
        assertEquals(true, Tokenizer.isOperator("*"));
        assertEquals(true, Tokenizer.isOperator("/"));
        
    }
    /*
    @Test
    public void testIsDelComma() {
        System.out.println("isComma");
        assertEquals(true, Tokenizer.isDelComma(','));
        assertEquals(true, Tokenizer.isDelComma('+'));
        
    }
    @Test
    public void testIsRightPar() {
        System.out.println("isRightPar");
        assertEquals(true, Tokenizer.isRightPar(')'));
        assertEquals(true, Tokenizer.isRightPar('+'));
        
    }
    @Test
    public void testIsLeftPar() {
        System.out.println("isLeftPar");
        assertEquals(true, Tokenizer.isLeftPar('('));
        assertEquals(true, Tokenizer.isLeftPar('+'));
        
    }
    */

    /**
     * Test of isNumber method, of class Tokenizer.
     */
    
}
